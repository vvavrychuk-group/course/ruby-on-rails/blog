json.extract! post, :id, :title, :body, :date_added, :created_at, :updated_at
json.url post_url(post, format: :json)
