class CommentsController < ApplicationController
  before_action :set_post

  # POST /posts/:post_id/comments
  # POST /posts/:post_id/comments.json
  def create
    @post.comments.create!(comments_params)
    redirect_to @post
  end

  private
    def set_post
      @post = Post.find(params[:post_id])
    end

    def comments_params
      params.require(:comment).permit(:body)
    end
end
